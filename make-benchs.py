import collections
import filecmp
import glob
import json
import os
import re
import shutil
import tempfile
import zipfile

from xml.dom import minidom
from xml.etree import ElementTree


GIT_REPO = 'https://gitlab.com/sosy-lab/software/ldv-klever-benchmarks'
# Tracking specific verification job identifiers within this test suit does not have any sense since corresponding
# verification jobs do not live for a long period of time. From the other side these identifiers change each time when
# the test suit is re-generated. By replacing specific verification job identifiers with the common one we can easily
# avoid the necessity to make corresponding updates to sources frequently.
JOB_ID = '00000000-0000-0000-0000-000000000000'

# Some verification tasks cause regressions, e.g. due to they may need about the same amount of memory as the memory
# limit. Exclude them to have a chance to focus on real bad cases.
VERIFICATION_TASK_BLACK_LIST = [
    'Linux-Loadable-kernel-modules-sample/drivers---ata---pata_arasan_cf.ko-memory-safety.yml',
    'Linux-Testing-Requirement-specifications/2-ext-modules---linux---alloc---usb-lock---safe.ko-alloc-usb-lock.yml',
    'Linux-Validation-Stable-Branch-Bugs-for-2014-Year/10-drivers---spi---spi-imx.ko-drivers-clk1.yml',
    'Linux-Validation-Stable-Branch-Bugs-for-2014-Year/12-drivers---staging---iio---adc---mxs-lradc.ko-drivers-clk1.yml',
    'Linux-Testing-Requirement-specifications/1-ext-modules---linux---alloc---spinlock---safe.ko-alloc-spinlock.yml',
    'Linux-Testing-Testing-verifiers/0-ext-modules---lists---create-insert-data-1-insert-data-2.ko-test-common.yml',
    # Our version of CPAchecker SMG, that currently lives in a branch, solves this task in ~10 minutes, while the trunk
    # version needs more time that sometimes causes timeouts.
    'Linux-Validation-Bugs-found-by-Klever/42-drivers---media---platform---qcom---camss---qcom-camss.ko-memory-safety.yml',
    # Following 2 tasks are flaky. There is either timeout or out of Java memory for them from time to time.
    'Linux-Validation-Bugs-found-by-Klever-but-fixed-by-the-Linux-kernel-developers-themselves/0-drivers---media---pci---bt8xx---bttv.ko-memory-safety.yml',
    'Linux-Validation-Bugs-found-by-Klever-but-fixed-by-the-Linux-kernel-developers-themselves/1-drivers---media---pci---bt8xx---bttv.ko-memory-safety.yml',
    # This task levearages an option that is supported only in the branch with CPAchecker SMG. Moreover, this option
    # crashes the whole test suite at the validation step since "*" is not allowed.
    'Linux-Testing-Testing-verifiers/3-ext-modules---initializers---strings.ko-test-memory-checking-without-uncertainty.yml'
]
# Track that all verification task from the black list does exist. It may be not the case, e.g. when there is a misprint
# in a task name in the black list.
task_black_list = {task: False for task in VERIFICATION_TASK_BLACK_LIST}

# Find out available property files. Later they will be matched by property files of individual verification tasks.
prp_file_names = []
for prp_file_name in os.listdir('properties'):
    prp_file_names.append(os.path.join('properties', prp_file_name))

# It is important to use exactly the same options for verification tasks, otherwise, verification results may differ.
# From the other side, we can not generate benchmarks for individual verification tasks due to efficiency reasons.
# Hopefully, many verification tasks use the same sets of options and it is possible to specify options for groups of
# verification tasks. So, build a map from sets of options to corresponding lists of verification tasks.
with open('job-task-opt-sets.json') as fp:
    job_task_opt_sets = json.load(fp)

# There will be a separate benchmark for concurrency safety since CPALockator lives in branch while CI works just with
# trunk. Perhaps, one day CPALockator will be in trunk.
with open('job-task-opt-sets-concurrency-safety.json') as fp:
    job_task_opt_sets_concurrency_safety = json.load(fp)

# Ditto for CPAchecker SMG except for some its version is in trunk, but also there is another version that is under
# active development
with open('job-task-opt-sets-memory-safety.json') as fp:
    job_task_opt_sets_memory_safety = json.load(fp)

job_id_pattern = re.compile(r'[-0-9a-fA-F]{36}')

# Handle archives with verifier input files. Each archive corresponds to a single verification jobs.
for verifier_input_files_archive in glob.glob('*.zip'):
    print('Extract verifier input files archive "{0}"'.format(verifier_input_files_archive))
    with tempfile.TemporaryDirectory() as td:
        with zipfile.ZipFile(verifier_input_files_archive) as zfp:
            zfp.extractall(td)

        # We assume that there is the only job and version within each archive.
        job_dir = os.listdir(td)[0]
        print('Process verifier input files for verification job "{0}"'.format(job_dir))

        # Either add verification tasks for a new job or overwrite previously added ones.
        job_task_opt_sets[job_dir] = {}
        job_task_opt_sets_concurrency_safety[job_dir] = {}
        job_task_opt_sets_memory_safety[job_dir] = {}

        stripped_job_dir = job_dir.replace(' - ', '-').replace(' ', '-')

        # Drop all former verification tasks for a given job.
        if os.path.exists(stripped_job_dir):
            shutil.rmtree(stripped_job_dir)

        os.makedirs(stripped_job_dir)

        # There may be the same verification tasks within subdirectory for "Unsafes" and "Unknowns", e.g. when after
        # reporting several Unsafes the verification tool was terminated and the overall verdict is Unknown. Skip
        # such verification tasks corresponding to Unknowns.
        stripped_names = set()
        for verdict in ('Safes', 'Unsafes', 'Unknowns'):
            verdict_dir = os.path.join(td, job_dir, verdict)
            # There may be no some verdicts for a given job.
            if not os.path.isdir(verdict_dir):
                continue

            print('Process "{0}"'.format(verdict))

            for verification_task in os.listdir(verdict_dir):
                print('Process verification task "{0}"'.format(verification_task))

                name, ext = os.path.splitext(verification_task)
                stripped_name = name.replace(' - ', '-').replace(':', '-').replace(' ', '-')
                if stripped_name in stripped_names:
                    print('Skip already handled "{0}"'.format(stripped_name))
                    continue
                stripped_names.add(stripped_name)
                src_file_name = stripped_name + '.cil.i'
                yml_file_name = stripped_name + '.yml'
                prp_file_name = None

                # Determine checked requirement specification.
                requirement_specification = name.split(' - ')[-1]

                cur_task_opt_sets_to_add = []
                if requirement_specification.startswith('concurrency safety'):
                    cur_task_opt_sets_to_add.append(job_task_opt_sets_concurrency_safety[job_dir])
                # Add memory safety verification tasks to both specific and the common set.
                elif requirement_specification.startswith('memory safety'):
                    cur_task_opt_sets_to_add.append(job_task_opt_sets_memory_safety[job_dir])
                    cur_task_opt_sets_to_add.append(job_task_opt_sets[job_dir])
                else:
                    cur_task_opt_sets_to_add.append(job_task_opt_sets[job_dir])

                # Extract archive and handle all meaningful files from it.
                with tempfile.TemporaryDirectory() as vtd:
                    with zipfile.ZipFile(os.path.join(verdict_dir, verification_task)) as zfp:
                        zfp.extractall(vtd)

                    # Get rid of most variable part of line directives that is verification job identifiers.
                    shrinked_cil_lines = []
                    with open(os.path.join(vtd, 'cil.i')) as fp:
                        for line in fp.readlines():
                            shrinked_cil_lines.append(re.sub(job_id_pattern, JOB_ID, line))

                    with open(os.path.join(stripped_job_dir, src_file_name), 'w') as fp:
                        for line in shrinked_cil_lines:
                            fp.write(line)

                    with open(os.path.join(vtd, 'safe-prps.prp')) as fp:
                        prp = fp.read()
                    for prp_file_name_candidate in prp_file_names:
                        if filecmp.cmp(os.path.join(vtd, 'safe-prps.prp'), prp_file_name_candidate):
                            prp_file_name = prp_file_name_candidate
                            break

                    opts_set = collections.OrderedDict()
                    tree = ElementTree.parse(os.path.join(vtd, 'benchmark.xml'))
                    root = tree.getroot()
                    for rundefinition in root.findall('rundefinition'):
                        for opt in rundefinition.findall('option'):
                            opt_name = opt.attrib.get('name')
                            # Take into account that XML allows multiple values for option with the same name.
                            if opt_name not in opts_set:
                                opts_set[opt_name] = []
                            opts_set[opt_name].append(opt.text)
                    opts_set_str = json.dumps(opts_set)
                    for cur_task_opt_sets in cur_task_opt_sets_to_add:
                        if opts_set_str not in cur_task_opt_sets:
                            # We suggest that there is the same property file for all YAML files for a given set of
                            # options.
                            cur_task_opt_sets[opts_set_str] = {
                                'property file': prp_file_name,
                                'YAML files': []
                            }
                        cur_task_opt_sets[opts_set_str]['YAML files'].append(os.path.join(stripped_job_dir,
                                                                                          yml_file_name))

                if not prp_file_name:
                    raise RuntimeError('Unsupported property found:\n{0}'.format(prp))

                with open(os.path.join(stripped_job_dir, yml_file_name), 'w') as fp:
                    fp.write("format_version: '1.0'\n\n")
                    fp.write("input_files: '{0}'\n\n".format(src_file_name))
                    fp.write("properties:\n  - property_file: {0}\n"
                             .format(os.path.relpath(prp_file_name, stripped_job_dir)))
                    # We do not know expected verdicts for unknowns and it is not easy to find them out from Klever. But
                    # CI still can produce something useful for unknowns, e.g. when there are expectations instead of
                    # timeouts or v.v.
                    if verdict == 'Safes':
                        fp.write("    expected_verdict: true\n")
                    elif verdict == 'Unsafes':
                        fp.write("    expected_verdict: false\n")


def generate_benchmark(job_task_opt_sets, benchmark_file_suffix=''):
    # Create benchmark. Try to sort the data as much as possible to avoid big diffs with previous versions of this file.
    benchmark = ElementTree.Element('benchmark', {
        'tool': 'cpachecker',
        'timelimit': '810 s',
        'hardtimelimit': '900 s',
        # Don't forget to change value for option -heap below if you are going to change memory limit here.
        'memlimit': '3 GB',
        'cpuCores': '1'
    })

    comment = ElementTree.Comment('This benchmark was generated automatically by "{0}" from the Git repository at {1}.'
                                  ' Please, do not edit this benchmark manually if you find it inconsistent.'
                                  ' Instead you should fix that script.'
                                  .format(os.path.basename(__file__), GIT_REPO))
    benchmark.append(comment)

    ElementTree.SubElement(benchmark, 'rundefinition')

    for job_name in sorted(job_task_opt_sets.keys()):
        task_opt_sets = job_task_opt_sets[job_name]

        if not task_opt_sets:
            continue

        comment = ElementTree.Comment('Verification tasks of verification job "{0}".'.format(job_name))
        benchmark.append(comment)

        opt_set_strs = sorted(task_opt_sets.keys())
        for opts_set_str in opt_set_strs:
            tasks = ElementTree.SubElement(benchmark, 'tasks')

            # Specify options for a given set of verification tasks.
            opts_set = json.loads(opts_set_str)
            for opt_name, opt_values in opts_set.items():
                for opt_value in opt_values:
                    if opt_name == '-heap':
                        opt_value = '2400m'
                    ElementTree.SubElement(tasks, 'option', {'name': opt_name}).text = opt_value

            # Specify YAML files requiring corresponding options.
            for yml_file_name in sorted(task_opt_sets[opts_set_str]['YAML files']):
                if yml_file_name in task_black_list:
                    print('Skip verification task "{0}" as it is included into the black list'.format(yml_file_name))
                    task_black_list[yml_file_name] = True
                    continue
                ElementTree.SubElement(tasks, 'include').text = yml_file_name

            ElementTree.SubElement(tasks, "propertyfile").text = task_opt_sets[opts_set_str]['property file']

    with open('benchmark{0}.xml'.format(benchmark_file_suffix), 'w') as fp:
        fp.write(minidom.parseString(ElementTree.tostring(benchmark)).toprettyxml(indent="    "))


generate_benchmark(job_task_opt_sets)
generate_benchmark(job_task_opt_sets_concurrency_safety, '-concurrency-safety')
generate_benchmark(job_task_opt_sets_memory_safety, '-memory-safety')

# Sort YAML files within JSON files since this is not achieved by sort_keys=True below.
for cur_job_task_opt_sets in (job_task_opt_sets, job_task_opt_sets_concurrency_safety):
    for task_opt_sets in cur_job_task_opt_sets.values():
        for task_opt_set in task_opt_sets:
            task_opt_sets[task_opt_set]['YAML files'] = sorted(task_opt_sets[task_opt_set]['YAML files'])

with open('job-task-opt-sets.json', 'w') as fp:
    json.dump(job_task_opt_sets, fp, ensure_ascii=False, sort_keys=True, indent=4)

with open('job-task-opt-sets-concurrency-safety.json', 'w') as fp:
    json.dump(job_task_opt_sets_concurrency_safety, fp, ensure_ascii=False, sort_keys=True, indent=4)

with open('job-task-opt-sets-memory-safety.json', 'w') as fp:
    json.dump(job_task_opt_sets_memory_safety, fp, ensure_ascii=False, sort_keys=True, indent=4)

missed_verification_tasks = []
for task, was_found in task_black_list.items():
    if not was_found:
        missed_verification_tasks.append(task)

if missed_verification_tasks:
    raise RuntimeError('Following verification tasks are in the black list but they do not exist:\n{0}'
                       .format('\n'.join(['  {0}'.format(task) for task in missed_verification_tasks])))
