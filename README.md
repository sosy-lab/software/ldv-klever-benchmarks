This repository contains benchmarks corresponding to Klever regression integrity test suites.

Benchmarks and all the necessary stuff are generated automatically on the basis of archives that one can get from Klever
after it completes decision of its testing/validation verification jobs.
Just limits for computational resources are hard coded within the script since they do not correspond to limits from
archives.
This is the case because of there is not an ability to specify various limits for different verification tasks within
the same benchmark while having multiple benchmarks cause too considerable overhead for CI.
Also, you may need to add additional property files into directory "properties" if your verification tasks will suddenly
refer something that is not presented there.
